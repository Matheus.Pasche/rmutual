# Nosso novo pacote chegou! :computer: 

O RMutual é uma versão atualizada, enxuta e modificada do MutualR. Agora, o env e o dai ficam na pasta compartilhada, em bases gerais. Para funcionários novos ou reset, deve-se instalar o dai antes do RMutual.

```
devtools::install_gitlab('matheus.pasche/RMutual')
```

 As funções podem ser divididas em 3 tipos:

## Conexões:
Carrega o env, como já fazia. Está em desenvolvimento a autenticação automática para API Google.
```
RMutual::getEnv()
```
Conecta-se com o H20.ai
```
RMutual::daiConnect()
```
Conecta aos Report Service
```
RMutual::GetComRSProd() 
```
Conexões ao mongo: a função é o nome da db. Basta inserir a collection.
```
RMutual::getScoreMongoProd (collection = "Score")
```
Realiza o setup do slack
```
RMutual::slackSetup()
```

## H2O.ai:
Splita um data frame com base na safra e o envia ao H2O
```
RMutual::splitTrain()
```
Faz a avaliação dos modelos pelo KS
```
RMutual::evalModel()
```
Utiliza as duas funções acima para separar, treinar e avaliar modelos.
```
RMutual::trainModel() 
```

## Utilidades:
Soma, média, sd e medianas já sem NA
```
RMutual::sum()
```
Corrige o CPF 
```
RMutual::cpfFix()
```
Operador de negação do %in%
```
RMutual::%not_in% 
```

## ggplot2:
cria paleta de cores e estilo
```
RMutual::scale_color_mutual()
RMutual::scale_fill_mutual()
```
