% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/sum.R
\name{sum}
\alias{sum}
\title{Sum}
\usage{
sum(x, na.rm = T)
}
\arguments{
\item{x}{Vetor}

\item{na.rm}{remove NA}
}
\description{
seta na.rm = T como padrão
}
\examples{
sum(c(1,4,5,NA))

}
