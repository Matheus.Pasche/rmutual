#' Slack setup
#' @param channel canal desejado
#' @export
#'
slackSetup <- function(channel) {
  tryCatch(expr = {
    slackr::slackr_setup(
      channel = {{channel}},
      username = Sys.getenv("username"),
      token  = 'xoxb-259422545825-1023992005926-mPAP6IqLqwy8xTHjHneaWDjE',
      incoming_webhook_url = Sys.getenv("incoming_webhook_url")
    )

    logger::log_success("Connection with Slack set up")
  },
  error = function(e) {
    logger::log_error("Could not set up connection with Slack. Try again.")
  })
}
