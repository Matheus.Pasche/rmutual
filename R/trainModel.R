#'  Treinar modelo no H2O
#'
#' @param .data data frame a ser modelado
#' @param experiment_name nome do experimento
#' @param scorer o scorer a ser usado para validar o modelo
#' @param keep_files onde os arquivos ficarao salvos
#' @param quantis quantis para realizar o eval do modelo
#' @param target_col coluna alvo do modelo, precisa ser uma string e não um símbolo
#' @param interpretability interpretabilidade do modelo
#' @param time tempo de estimação
#' @param accuracy parametro de precisão
#' @param is_classification TRUE para class., FALSE para reg
#' @param is_timeseries default FALSE,
#' @param weight_col coluna de peso
#' @param cols_to_drop colunas para dropar no momento da estimação
#' @param date_split data referencia para split de treino e teste
#' @param seed seed
#' @param progress barra de progresso
#'
#'
#' @export
#' @importFrom magrittr "%<>%"
#' @import dai
#' @import stringr
#' @import dplyr
#' @importFrom purrr map
#' @return
#' @export
trainModel <- function(.data,
                       experiment_name,
                       scorer = "GINI",
                       keep_files,
                       quantis = 20,
                       target_col = 'EVER60em6',
                       accuracy = 2,
                       time = 3,
                       interpretability = 8,
                       weight_col = 'peso',
                       cols_to_drop = c('cpf', 'safra', 'status'),
                       date_split = '2019-11-01',
                       seed= 1234,
                       is_classification = TRUE,
                       is_timeseries = FALSE,
                       progress = TRUE)

{

  if(is.null(target_col)) {

    logger::log_error("Variable `target_col` must have a string with variable name.")
    stop()

  }


splitTrain(.data = .data, experiment_name = experiment_name ,keep_files = keep_files,date_col = date_col, date_split =date_split, seed = seed)


 tryCatch(expr = {


model <-    dai::dai.train(
                   training_frame = dai::dai.get_frame(dai::dai.list_datasets()[2,1]),
                   testing_frame = dai::dai.get_frame(dai::dai.list_datasets()[3,1]),
                   target_col = target_col,
                   interpretability = interpretability,
                   time = time,
                   accuracy = accuracy,
                   weight_col = weight_col,
                   cols_to_drop = cols_to_drop,
                   scorer = scorer,
                   seed = seed,
                   experiment_name = experiment_name,
                   progress = progress,
                   is_classification = T,
                   is_timeseries = is_timeseries)
  },



  error = function(e) {
    logger::log_error("Could not train specified model")
  })

logger::log_info('Model trained successfully')

eval <- map(list.files(paste0(keep_files, experiment_name, '/'), full.names = T), ~
              readr::read_csv(.x, show_col_types = FALSE) %>%
              filter(status == 'efetivo') %>%
              bind_cols(predict(model, newdata = dai::as.DAIFrame(.))) %>%
              evalModel(., quantis = {{quantis}}))

names(eval) = c('KS Treino+Teste', 'KS Teste', 'KS Treino')
model <- list(model, eval)
names(model) = c('modelo', 'KS Eval')


return(model)
}
