#' Median
#' @description seta na.rm = T como padrão
#' @param x Vetor
#' @param na.rm Remover NA padrão para T
#' @export
#'
#' @examples
#' median(c(1,4,5,NA))
#'
median <- function(x, na.rm = T){

  stats::median(x, na.rm = na.rm)

}
