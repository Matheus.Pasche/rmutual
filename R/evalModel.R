#' evalModel
#' @description Avalia o EVER60em6 por quantis. Implementação da função ks_eval_puro do Gustavo.
#' @param df Data frame com as escoragens. Deve ter, no mínimo, três colunas: EVER60em6,EVER60em6.0,EVER60em6.1.
#' @param quantis Número de quantis de quebra. Default para 20.
#'
#' @return
#' @export
#'
evalModel <- function(df, quantis = 20) {


  test_preds <- df %>%
    dplyr::filter(!is.na(EVER60em6)) %>%
    dplyr::mutate(quantile = ntile(1-EVER60em6.1, {{quantis}}))  %>%
    dplyr::group_by(quantile) %>%
    dplyr::summarise(score_min = min(1-EVER60em6.1)*1000,
              score_max = max(1-EVER60em6.1)*1000,
              Mau = sum( EVER60em6 ),
              count = n(),
              Bom = count - Mau) %>%
    dplyr::ungroup() %>%
    dplyr::mutate(Bom_count = sum(Bom),
           Mau_count = sum(Mau),
           pct_Bom = 100*(Bom/Bom_count),
           pct_Mau = 100*(Mau/Mau_count),
           cum_Bom = cumsum(pct_Bom),
           cum_Mau = cumsum(pct_Mau),
           ks      = cum_Mau - cum_Bom,
           EVER = Mau/count) %>%
    dplyr::select(quantile,count,Mau,Bom,score_min,score_max,pct_Bom,pct_Mau,cum_Bom,cum_Mau,ks,EVER)


}
